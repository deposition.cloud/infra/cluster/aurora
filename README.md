# Aurora

> a natural electrical phenomenon characterized by the appearance of streamers of coloured light in the sky

Base layer of cluster services for an operational cloud.

- cluster admins
- nginx ingress controller
- cert-manager
- health check with an nginx deployment: [health.deposition.cloud](https://health.deposition.cloud)

## Prerequisites

Requires an Internet facing MicroK8s cluster.

## Design

2 clusters on the same LAN.

The `live` cluster is the production cloud, a 3+ nodes high-availability bare metal setup.

The `omega` cluster is a few commits ahead and doubles as a fail-over 1-node VM cluster.

## Develop and Deploy

``` bash
pulumi config set kubernetes:context live # or
pulumi config set kubernetes:context omega 
```

Pre-requisites for Helm.

``` bash
helm repo add stable https://charts.helm.sh/stable
helm repo add bitnami https://charts.bitnami.com/bitnami
```

### Pulumi Stack Config

Ensure you have all the required configuration variables setup

``` bash
pulumi config set --plaintext domain deposition.cloud # replace with your domain
pulumi config set --plaintext adminEmail reply@deposition.cloud
```

Enable the desired features in `Pulumi.<stackName>.yaml`

``` yaml
config:
  ...
  aurora:features:
    certmanager: true
    dashboard: true
    health: true
    ingress: true
  ...
```

### NGINX Ingress Controller

``` bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
```

### Cert Manager

``` bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
```

#### Google CloudDNS

cert-manager needs to be able to add records to CloudDNS in order to solve the DNS01 challenge.

Prerequisites:

```bash
sudo snap install google-cloud-cli --classic
gcloud auth login
```


``` bash
PROJECT_ID=deposition-cloud # gcp-project-id
gcloud config set project $PROJECT_ID
gcloud iam service-accounts create dns01-solver --display-name "dns01-solver"
gcloud projects add-iam-policy-binding $PROJECT_ID --member serviceAccount:dns01-solver@$PROJECT_ID.iam.gserviceaccount.com --role roles/dns.admin
gcloud iam service-accounts keys create ca/key.json --iam-account dns01-solver@$PROJECT_ID.iam.gserviceaccount.com
cat ca/key.json | base64 -w 0 | pulumi config set --secret certmanagerGcpCloudDnsServiceAccountKey
```

Ref: https://cert-manager.io/docs/configuration/acme/dns01/google/

#### Use Production Issuer

> TODO - what we choose here will be propagated to dependent stacks

### Deploy

``` bash
pulumi up -f -y
```

We should now be able to securely access: [health.deposition.cloud](https://health.deposition.cloud)

Yey! :fireworks:

## Postresquisites



## Troubleshooting

Many things could go wrong. :smile:
