import * as pulumi from '@pulumi/pulumi'
import * as random from "@pulumi/random";
import * as kubernetes from '@pulumi/kubernetes'
import * as cert from '@pulumi/kubernetes-cert-manager'
import * as fs from 'fs';
import * as path from 'path';

// Function to read and base64 encode file content
function encodeFileContent (filePath: string): string {
  const fileContent = fs.readFileSync(filePath);
  return Buffer.from(fileContent).toString('base64');
}

const rookCephToolboxScriptPath = path.join(__dirname, 'scripts', 'rook-ceph-toolbox.sh');
const rookCephToolboxScript = fs.readFileSync(rookCephToolboxScriptPath, 'utf8');

const config = new pulumi.Config();
const username = config.require("username");
const externalIp = config.require("externalIp");
const publicHttpPort = config.get("publicHttpPort");
const publicHttpsPort = config.get("publicHttpsPort");

const stack = pulumi.getStack();
const org = pulumi.getOrganization();

const dam = new pulumi.StackReference(`${org}/dam/${stack}`);

const damOutput: pulumi.Output<{
  kubeconfig: string
}> = dam.getOutput("out") as pulumi.Output<{
  kubeconfig: string
}>;

const http = 80
const https = 443
const kubeSystemNamespaceName = 'kube-system'

interface Features {
  ingress: boolean
  certmanager: boolean
  secure: boolean
  alive: boolean
  dashboard: boolean
  rook?: {
    ceph: string,
  }
  preflight?: {
    subdomain: string,
    ip: string
  }
}

const tier = pulumi.getProject()

const features = config.requireObject<Features>('features')

interface ClusterAdmins {
  paul: boolean
  eric: boolean
}

const clusterAdmins = config.getObject<Features>('clusterAdmins') ?? {}

const domain = config.require('domain')
const adminEmail = config.require('adminEmail')
const issuer = config.require('issuer')
console.log(`Booting up ${domain} with ${issuer} issuer for ${adminEmail}...`)


const kubeconfig = damOutput.kubeconfig

let k8s = new kubernetes.Provider("mk8s", {
  kubeconfig
});

const customContent = `
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Links Page</title>
</head>
<body>
<h1>${org}/${stack}</h1>
<ul>
    <li>externalIp: ${externalIp}</li>
    <li>preflight ip: ${features.preflight?.ip}</li>
</ul>

<h2>On This Cluster</h2>
<ul>
    <li><a href="http://alive.deposition.cloud${publicHttpPort ? ':' + publicHttpPort : ''}/" target="_blank">http://alive.deposition.cloud${publicHttpPort ? ':' + publicHttpPort : ''}/</a></li>
    <li><a href="https://secure.deposition.cloud${publicHttpPort ? ':' + publicHttpPort : ''}/" target="_blank">https://secure.deposition.cloud${publicHttpPort ? ':' + publicHttpPort : ''}/</a></li>
</ul>

<h2>Preflight Cluster</h2>
<ul>
    <li><a href="http://alive.omega.deposition.cloud${publicHttpPort ? ':' + publicHttpPort : ''}/" target="_blank">http://alive.omega.deposition.cloud${publicHttpPort ? ':' + publicHttpPort : ''}/</a></li>
    <li><a href="https://secure.omega.deposition.cloud${publicHttpPort ? ':' + publicHttpPort : ''}/" target="_blank">https://secure.omega.deposition.cloud${publicHttpPort ? ':' + publicHttpPort : ''}/</a></li>
</ul>

</body>
</html>
`;

const output: {
  admins: pulumi.Output<string>[],
  alive?: {
    url: string,
    bash?: string
  },
  clusterIssuer?: kubernetes.apiextensions.CustomResource,
  secure?: {
    url: string,
    bash?: string
  },
  dashboard?: string
} = { admins: [] }

for (let [serviceAccountShortName, active] of Object.entries(clusterAdmins)) {
  console.log(serviceAccountShortName)
  if (active) {
    const serviceAccount = new kubernetes.core.v1.ServiceAccount(serviceAccountShortName, {
      metadata: { namespace: kubeSystemNamespaceName, labels: { stack, tier } }
    }, {
      provider: k8s,
      parent: k8s
    })

    const privilegedCRB = new kubernetes.rbac.v1.ClusterRoleBinding(`${serviceAccountShortName}-cluster-admin-binding`, {
      roleRef: {
        apiGroup: 'rbac.authorization.k8s.io',
        kind: 'ClusterRole',
        name: 'cluster-admin'
      },
      subjects: [{
        kind: 'User',
        name: serviceAccount.metadata.name,
        apiGroup: 'rbac.authorization.k8s.io'
      }]
    }, {
      provider: k8s,
      parent: serviceAccount,
      dependsOn: serviceAccount
    })

    const serviceAccountSecret = new kubernetes.core.v1.Secret(`${serviceAccountShortName}-secret`, {
      metadata: {
        namespace: kubeSystemNamespaceName,
        labels: { stack, tier }
      },
      type: "Opaque",
      data: {
        'token': new random.RandomPassword(`${serviceAccountShortName}-token`, { length: 64, special: true }).result.apply(result => Buffer.from(result).toString('base64')),
      }
    }, {
      provider: k8s,
      parent: privilegedCRB,
      dependsOn: privilegedCRB
    });

    output.admins.push(pulumi.unsecret(pulumi.interpolate`kubectl get secret/${serviceAccountSecret.metadata.name} -n kube-system -o go-template='{{.data.token | base64decode}}'`))
  }
}

/*
* rook ceph
*/

if (features.rook) {
  const rookCephShortName = 'rook'
  const rookCephPrefix = 'rook-ceph'
  const rookCephNamespace = new kubernetes.core.v1.Namespace(rookCephPrefix, {
    metadata: {
      name: rookCephPrefix,
      labels: { name: rookCephPrefix, stack, tier }
    }
  }, {
    provider: k8s
  })

  const rookCephOperator = new kubernetes.helm.v3.Release(rookCephShortName, {
    chart: 'rook-ceph',
    repositoryOpts: {
      repo: 'https://charts.rook.io/release'
    },
    namespace: rookCephNamespace.metadata.name,
    name: rookCephShortName,
    values: {
      csi: {
        allowUnsupportedVersion: true,
        kubeletDirPath: '/var/snap/microk8s/common/var/lib/kubelet'
      }
    }
  }, {
    provider: k8s,
    parent: rookCephNamespace
  })

  const rookCephClusterShortName = 'rook-ceph-cluster'

  const rookCephCluster = new kubernetes.helm.v3.Release(rookCephClusterShortName, {
    chart: 'rook-ceph-cluster',
    repositoryOpts: {
      repo: 'https://charts.rook.io/release'
    },
    namespace: rookCephNamespace.metadata.name,
    name: rookCephClusterShortName,
    values: {
      operatorNamespace: rookCephNamespace.metadata.name,
      cephClusterSpec: {
        external: {
          enable: true
        },
        crashCollector: {
          disable: true
        },
        healthCheck: {
          daemonHealth: {
            mon: {
              disabled: false,
              interval: '45s'
            }
          }
        }
      }
    }
  }, {
    provider: k8s,
    parent: rookCephNamespace,
    dependsOn: rookCephNamespace
  })

  const rookCephToolsDeployment = new kubernetes.apps.v1.Deployment("rook-ceph-tools", {
    metadata: {
      name: "rook-ceph-tools",
      namespace: rookCephNamespace.metadata.name,
      labels: {
        app: "rook-ceph-tools",
      },
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: {
          app: "rook-ceph-tools",
        },
      },
      template: {
        metadata: {
          labels: {
            app: "rook-ceph-tools",
          },
        },
        spec: {
          dnsPolicy: "ClusterFirstWithHostNet",
          tolerations: [{
            key: "node.kubernetes.io/unreachable",
            operator: "Exists",
            effect: "NoExecute",
            tolerationSeconds: 5,
          }],
          containers: [{
            name: "rook-ceph-tools",
            image: "quay.io/ceph/ceph:v17.2.6",
            command: ["/bin/bash", "-c", rookCephToolboxScript],
            imagePullPolicy: "IfNotPresent",
            tty: true,
            securityContext: {
              runAsNonRoot: true,
              runAsUser: 2016,
              runAsGroup: 2016,
              capabilities: {
                drop: ["ALL"],
              },
            },
            env: [{
              name: "ROOK_CEPH_USERNAME",
              valueFrom: {
                secretKeyRef: {
                  name: "rook-ceph-mon",
                  key: "ceph-username",
                },
              },
            }],
            volumeMounts: [
              { mountPath: "/etc/ceph", name: "ceph-config" },
              { mountPath: "/etc/rook", name: "mon-endpoint-volume" },
              { mountPath: "/var/lib/rook-ceph-mon", name: "ceph-admin-secret", readOnly: true },
            ],
          }],
          volumes: [
            {
              name: "ceph-admin-secret",
              secret: {
                secretName: "rook-ceph-mon",
                optional: false,
                items: [{ key: "ceph-secret", path: "secret.keyring" }],
              },
            }, {
              name: "mon-endpoint-volume",
              configMap: {
                name: "rook-ceph-mon-endpoints",
                items: [{ key: "data", path: "mon-endpoints" }],
              },
            }, {
              name: "ceph-config",
              emptyDir: {},
            },
          ],
        },
      },
    },
  }, {
    provider: k8s,
    parent: rookCephNamespace,
    dependsOn: rookCephNamespace
  });

  // const pv = new kubernetes.core.v1.PersistentVolume("ceph-pv", {
  //   spec: {
  //     capacity: {
  //       storage: "2Gi",
  //     },
  //     accessModes: ["ReadWriteOnce"]
  //   }
  // }, {
  //   provider: k8s,
  //   parent: rookCephOperator,
  //   dependsOn: rookCephCluster
  // });

  // const pvc = new kubernetes.core.v1.PersistentVolumeClaim("ceph-pvc", {
  //   spec: {
  //     accessModes: ["ReadWriteOnce"],
  //     storageClassName: "ceph-block",
  //     resources: {
  //       requests: {
  //         storage: "1Gi",
  //       },
  //     },
  //   },
  // }, {
  //   provider: k8s,
  //   parent: rookCephOperator,
  //   dependsOn: rookCephCluster
  // });

}

/*
* ingress
*/

if (features.ingress) {
  const ingressShortName = 'in'
  const ingressPrefix = 'ingress'
  const ingressNamespace = new kubernetes.core.v1.Namespace(ingressPrefix, {
    metadata: { labels: { name: ingressPrefix, stack, tier } }
  }, {
    provider: k8s
  })

  const ingress = new kubernetes.helm.v3.Release(ingressShortName, {
    chart: 'ingress-nginx',
    repositoryOpts: {
      repo: 'https://kubernetes.github.io/ingress-nginx'
    },
    namespace: ingressNamespace.metadata.name,
    name: ingressShortName,
    values: {
      controller: {}
    }
  }, {
    provider: k8s,
    parent: ingressNamespace
  })

  /*
  * alive check: can we reach a service via ingress?
  */

  if (features.alive) {
    const alivePrefix = 'alive'
    const aliveNamespace = new kubernetes.core.v1.Namespace(alivePrefix, {
      metadata: { labels: { name: alivePrefix, stack, tier } }
    }, {
      provider: k8s,
      dependsOn: [ingress]
    })
    const alive = aliveNamespace.metadata.name

    const aliveAppName = 'alive'
    const aliveAppLabels = { app: aliveAppName, stack, tier }


    const aliveConfigMap = new kubernetes.core.v1.ConfigMap(aliveAppName + '-config', {
      metadata: {
        namespace: alive,
        labels: aliveAppLabels,
      },
      data: { 'index.html': customContent },
    }, {
      provider: k8s,
      parent: aliveNamespace,
    });

    const aliveDeployment = new kubernetes.apps.v1.Deployment(
      aliveAppName, {
      metadata: {
        namespace: alive,
        labels: aliveAppLabels,
      },
      spec: {
        selector: { matchLabels: aliveAppLabels },
        replicas: 1,
        template: {
          metadata: { labels: aliveAppLabels },
          spec: {
            containers: [{
              name: aliveAppName,
              image: 'nginx',
              volumeMounts: [{  // Define the volume mount
                name: 'config-volume',
                mountPath: '/usr/share/nginx/html/index.html',  // Destination path in the container
                subPath: 'index.html',  // File in the ConfigMap
              }],
            }],
            volumes: [{  // Define the volume using the ConfigMap
              name: 'config-volume',
              configMap: {
                name: aliveConfigMap.metadata.name,
              },
            }],
          },
        }
      }
    }, {
      provider: k8s,
      parent: aliveConfigMap
    })

    const aliveService = new kubernetes.core.v1.Service(aliveAppName, {
      metadata: {
        namespace: alive,
        labels: aliveDeployment.spec.template.metadata.labels
      },
      spec: {
        type: 'ClusterIP',
        ports: [{ port: http, targetPort: http, protocol: 'TCP' }],
        selector: aliveAppLabels
      }
    }, {
      provider: k8s,
      parent: aliveNamespace
    })

    const aliveHost = `${aliveAppName}.${domain}`
    output.alive = {
      url: `http://${aliveHost}${publicHttpPort ? ':' + publicHttpPort : ''}`,
      bash: `curl -L -H 'Host: ${aliveHost}' ${externalIp}`
    }

    const aliveIngress = new kubernetes.networking.v1.Ingress(aliveAppName, {
      metadata: {
        namespace: alive,
        labels: aliveDeployment.spec.template.metadata.labels,
        annotations: {}
      },
      spec: {
        ingressClassName: 'nginx',
        rules: [{
          host: aliveHost,
          http: {
            paths: [{
              backend: {
                service: {
                  name: aliveService.metadata.name,
                  port: {
                    number: http
                  }
                }
              },
              path: '/',
              pathType: 'ImplementationSpecific'
            }]
          }
        }]
      }
    }, {
      provider: k8s,
      parent: aliveService
    })
  }

  /*
  * cert manager
  */

  if (features.certmanager) {
    const certmanagerName = 'vouch'
    const certmanagerNamespaceName = 'cert-manager'
    const certmanagerNamespace = new kubernetes.core.v1.Namespace(certmanagerNamespaceName, {
      metadata: {
        name: certmanagerNamespaceName,
        namespace: certmanagerNamespaceName,
        labels: { stack, tier }
      }
    }, {
      provider: k8s,
      parent: ingressNamespace
    })

    const certmanager = new cert.CertManager(certmanagerName, {
      installCRDs: true,
      helmOptions: {
        namespace: certmanagerNamespaceName,
        name: certmanagerName
      }
    }, {
      provider: k8s,
      parent: certmanagerNamespace
    })

    const clouddnsSecret = new kubernetes.core.v1.Secret('clouddns-dns01-solver-svc-acct', {
      metadata: {
        namespace: certmanagerNamespaceName
      },
      data: {
        'key.json': config.requireSecret('certmanagerGcpCloudDnsServiceAccountKey')
      }
    }, {
      provider: k8s,
      parent: certmanager
    })

    const clusterIssuer = new kubernetes.apiextensions.CustomResource('issuer', {
      apiVersion: 'cert-manager.io/v1',
      kind: 'ClusterIssuer',
      metadata: {
        labels: { stack, tier },
        name: issuer,
        namespace: certmanagerNamespaceName
      },
      spec: {
        acme: {
          server: (issuer === 'live') ? 'https://acme-v02.api.letsencrypt.org/directory' : 'https://acme-staging-v02.api.letsencrypt.org/directory',
          email: adminEmail,
          privateKeySecretRef: {
            name: issuer
          },
          solvers: [{
            dns01: {
              cloudDNS: {
                project: config.require('certmanagerGcpProjectId'),
                serviceAccountSecretRef: {
                  name: clouddnsSecret.metadata.name,
                  key: 'key.json'
                }
              }
            }
          }]
        }
      }
    }, {
      provider: k8s,
      parent: certmanager
    })

    output.clusterIssuer = clusterIssuer

    /*
    * health check for https and cert-manager
    */

    if (features.secure) {
      const securePrefix = 'secure'
      const secureNamespace = new kubernetes.core.v1.Namespace(securePrefix, {
        metadata: { labels: { name: securePrefix, stack, tier } }
      }, {
        provider: k8s,
        parent: certmanager,
        dependsOn: [ingress]
      })
      const secure = secureNamespace.metadata.name

      const secureAppName = 'secure'
      const secureAppLabels = { app: secureAppName, stack, tier }

      const secureConfigMap = new kubernetes.core.v1.ConfigMap(secureAppName + '-config', {
        metadata: {
          namespace: secure,
          labels: secureAppLabels,
        },
        data: { 'index.html': customContent },
      }, {
        provider: k8s,
        parent: secureNamespace,
      });

      const secureDeployment = new kubernetes.apps.v1.Deployment(
        secureAppName, {
        metadata: {
          namespace: secure,
          labels: secureAppLabels,
        },
        spec: {
          selector: { matchLabels: secureAppLabels },
          replicas: 1,
          template: {
            metadata: { labels: secureAppLabels },
            spec: {
              containers: [{
                name: secureAppName,
                image: 'nginx',
                volumeMounts: [{  // Define the volume mount
                  name: 'config-volume',
                  mountPath: '/usr/share/nginx/html/index.html',  // Destination path in the container
                  subPath: 'index.html',  // File in the ConfigMap
                }],
              }],
              volumes: [{  // Define the volume using the ConfigMap
                name: 'config-volume',
                configMap: {
                  name: secureConfigMap.metadata.name,
                },
              }],
            }
          }
        }
      }, {
        provider: k8s,
        parent: secureNamespace
      })

      const secureService = new kubernetes.core.v1.Service(secureAppName, {
        metadata: {
          namespace: secure,
          labels: secureDeployment.spec.template.metadata.labels
        },
        spec: {
          type: 'ClusterIP',
          ports: [{ port: http, targetPort: http, protocol: 'TCP' }],
          selector: secureAppLabels
        }
      }, {
        provider: k8s,
        parent: secureNamespace
      })

      const secureHost = `${secureAppName}.${domain}`
      output.secure = {
        url: `https://${secureHost}${publicHttpsPort ? ':' + publicHttpsPort : ''}`,
        bash: `curl -L -H 'Host: ${secureHost}' ${externalIp}`
      }

      const secureIngress = new kubernetes.networking.v1.Ingress(secureAppName, {
        metadata: {
          namespace: secure,
          labels: secureDeployment.spec.template.metadata.labels,
          annotations: {
            //'nginx.ingress.kubernetes.io/backend-protocol': 'https',
            //'nginx.ingress.kubernetes.io/ssl-passthrough': 'true',
            'cert-manager.io/cluster-issuer': issuer
          }
        },
        spec: {
          ingressClassName: 'nginx',
          tls: [{
            hosts: [secureHost],
            secretName: 'default-tls-secret'
          }],
          rules: [{
            host: secureHost,
            http: {
              paths: [{
                backend: {
                  service: {
                    name: secureService.metadata.name,
                    port: {
                      number: http
                    }
                  }
                },
                path: '/',
                pathType: 'ImplementationSpecific'
              }]
            }
          }]
        }
      }, {
        provider: k8s,
        parent: secureService
      })
    }

    /*
    * preflight proxy
    */

    if (features.preflight) {
      const preflightPrefix = 'preflight';
      const preflightNamespace = new kubernetes.core.v1.Namespace(preflightPrefix, {
        metadata: { labels: { name: preflightPrefix, stack, tier } }
      }, {
        provider: k8s,
        dependsOn: [ingress]
      });

      const preflight = preflightNamespace.metadata.name

      const preflightAppName = 'preflight'
      const preflightAppLabels = { app: preflightAppName, stack, tier }

      // Define the custom Nginx configuration as a string
      const customNginxConfig = `
upstream target_server {
  server ${features.preflight.ip};
}

server {
  server_name ~^(?<sub>.+)\\.omega\\.deposition\\.cloud$;

  location / {
    proxy_set_header Host $sub.deposition.cloud;
    proxy_pass http://target_server;
  }
}

server {
  server_name ~^(?<sub>.+)\\.omega\\.deposition\\.cloud$;

  ssl_certificate /etc/nginx/certs/your_site_crt_file.crt;
  ssl_certificate_key /etc/nginx/certs/your_site_crt_file.key;

  location / {
    proxy_set_header Host $sub.deposition.cloud;
    proxy_pass https://target_server;
    proxy_ssl_verify off;  # Disable SSL verification, useful for self-signed certificates
    proxy_ssl_server_name on;  # Pass the server name with SSL
  }
}
`;

      // Create a ConfigMap to hold the custom Nginx configuration
      const preflightNginxConfig = new kubernetes.core.v1.ConfigMap(preflightAppName + '-config', {
        metadata: {
          namespace: preflight,
          labels: preflightAppLabels
        },
        data: { 'custom-nginx.conf': customNginxConfig }
      }, {
        provider: k8s,
        parent: preflightNamespace
      });

      // Get the base64 encoded content of the certificate and key files
      const encodedCertContent = encodeFileContent('./ca/your_site_crt_file.crt');
      const encodedKeyContent = encodeFileContent('./ca/your_site_crt_file.key');

      const sslCertsSecret = new kubernetes.core.v1.Secret('ssl-certs', {
        metadata: {
          namespace: preflight,
          labels: preflightAppLabels,
        },
        type: 'Opaque',
        data: {
          'your_site_crt_file.crt': encodedCertContent,
          'your_site_crt_file.key': encodedKeyContent,
        },
      }, {
        provider: k8s,
        parent: preflightNginxConfig,
      });


      const preflightDeployment = new kubernetes.apps.v1.Deployment(
        preflightAppName, {
        metadata: {
          namespace: preflight,
          labels: preflightAppLabels,
        },
        spec: {
          selector: { matchLabels: preflightAppLabels },
          replicas: 1,
          template: {
            metadata: { labels: preflightAppLabels },
            spec: {
              containers: [{
                name: preflightAppName,
                image: 'nginx',
                volumeMounts: [{
                  name: 'config-volume',
                  mountPath: '/etc/nginx/conf.d',
                }, {
                  name: 'certs-volume',
                  mountPath: '/etc/nginx/certs',
                },],
              }],
              volumes: [{
                name: 'config-volume',
                configMap: {
                  name: preflightNginxConfig.metadata.name,
                },
              }, {
                name: 'certs-volume',
                secret: {
                  secretName: sslCertsSecret.metadata.name,
                }
              }]
            }
          }
        }
      }, {
        provider: k8s,
        parent: sslCertsSecret
      });

      // Create a ClusterIP service pointing to the above Endpoint
      const preflightService = new kubernetes.core.v1.Service('preflight', {
        metadata: {
          namespace: preflight,
          labels: preflightAppLabels,
        },
        spec: {
          type: 'ClusterIP',
          ports: [
            { name: 'http', port: http, protocol: 'TCP' },
            { name: 'https', port: https, protocol: 'TCP' },
          ],
          selector: preflightAppLabels
        },
      }, {
        provider: k8s,
        parent: preflightDeployment,
      });

      // Ingress with wildcard host matching
      const preflightIngress = new kubernetes.networking.v1.Ingress(preflightAppName, {
        metadata: {
          namespace: preflight,
          labels: preflightDeployment.spec.template.metadata.labels,
          annotations: {}
        },
        spec: {
          ingressClassName: 'nginx',
          rules: [{
            host: '*.omega.deposition.cloud',
            http: {
              paths: [{
                backend: {
                  service: {
                    name: preflightService.metadata.name,
                    port: {
                      number: http
                    }
                  }
                },
                path: '/',
                pathType: 'ImplementationSpecific'
              }]
            }
          }]
        }
      }, {
        provider: k8s,
        parent: preflightService
      })
    }


    /*
    * dashboard
    */

    if (features.dashboard) {
      const dashboardShortName = 'dash-basic'
      const kubernetesDashboardServiceName = 'kubernetes-dashboard'

      const dashboardHost = `${dashboardShortName}.${domain}`
      output.dashboard = `https://${dashboardHost}${publicHttpsPort ? ':' + publicHttpsPort : ''}`

      const dashboardIngress = new kubernetes.networking.v1.Ingress(dashboardShortName, {
        metadata: {
          namespace: kubeSystemNamespaceName,
          annotations: {
            'nginx.ingress.kubernetes.io/backend-protocol': 'https',
            'nginx.ingress.kubernetes.io/ssl-passthrough': 'true',
            'cert-manager.io/cluster-issuer': issuer
          },
          labels: { stack, tier }
        },
        spec: {
          ingressClassName: 'nginx',
          tls: [{
            hosts: [dashboardHost],
            secretName: 'default-tls-secret'
          }],
          rules: [{
            host: dashboardHost,
            http: {
              paths: [{
                backend: {
                  service: {
                    name: kubernetesDashboardServiceName,
                    port: { number: https }
                  },
                },
                path: '/',
                pathType: 'ImplementationSpecific'
              }]
            }
          }]
        }
      }, {
        parent: certmanager,
        dependsOn: [ingress]
      })
    }
  }
}

export const out: any = {
  features,
  kubeconfig: damOutput.kubeconfig,
  ...output
}
